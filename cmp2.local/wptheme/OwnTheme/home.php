<?php
/**
 * Created by PhpStorm.
 * User: felinedekeyser
 * Date: 10/03/17
 * Time: 09:42
 */

?>

<?php get_header(); ?>

<div class="container">
    <div class="row">
    <div class="col-md-10">
        <div class="card-columns">
        <?php if(have_posts())
            {
                while(have_posts())
                {
                //Initialize the post
                the_post();
        ?>
            <div class="card">
                <a href="<?php the_permalink() ?>" rel="Link naar item" title="Permalink naar <?php the_title_attribute(); ?>">
                    <div class="card-img-top img-fluid"><?php the_post_thumbnail(); ?></div>
                </a>
                <div class="card-block">
                    <h4 class="card-title"><?php the_title(); ?></h4>
                    <p class="card-text"><?php the_excerpt(); ?></p>
                    <a href="<?php the_permalink() ?>" rel="Link naar item" title="Permalink naar <?php the_title_attribute(); ?>">
                        Lees meer...
                    </a>
                    <div class="bewerkpost"><?php edit_post_link('Bewerken'); ?></div>
                </div>
            </div>
                    <?php
                    //Print the title and the content of the current post Zelfde als hierboven

                    //the_title('<h1>', '</h1>');
                    //the_excerpt();
                    //echo '<a href="';
                    //the_permalink();
                    //echo '">Lees meer</a>';
                }
            }
        else
        {
            echo 'No content available';
        } ?>
        </div>
        <nav class="pagination">
            <?php pagination_bar(); ?>
        </nav>

    </div>

        <aside class="col-md-2">
            <?php dynamic_sidebar( 'primary-sidebar' ); ?>
        </aside>
    </div>
</div>
<?php get_footer(); ?>