<?php
/**
 * Created by PhpStorm.
 * User: felinedekeyser
 * Date: 21/04/17
 * Time: 10:15
 */

function register_menu_locations() {
    register_nav_menus(
    array(
        'primary-menu' => __( 'Primary Menu' ),
        'footer-menu' => __( 'Footer Menu' ),
        'footer-social' => __( 'Footer Social' )
    )
);
}
add_action( 'init', 'register_menu_locations' );

add_theme_support( 'post-thumbnails' );

add_theme_support( 'custom-logo' );

function register_sidebar_locations() {
    /* Register the 'primary' sidebar. */
    register_sidebar(
        array(
            'id'            => 'primary-sidebar',
            'name'          => __( 'Primary Sidebar' ),
            'description'   => __( 'A short description of the sidebar.' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );
    register_sidebar(
    array(
        'id'            => 'footer-sidebar',
        'name'          => __( 'Footer Sidebar' ),
        'description'   => __( 'Sidebar in footer.' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    )
    );
    /* Repeat register_sidebar() code for additional sidebars. */
}
add_action( 'widgets_init', 'register_sidebar_locations' );

$header_info = array(
    'width'         => 1800,
    'height'        => 400,
    'default-image' => get_template_directory_uri() . '/images/AA.jpg',
    );
add_theme_support( 'custom-header', $header_info );

function custom_post_type_portfolio() {
    $labels = array(
        'name'               => _x( 'JupilerBOB-bieren', 'post type general name' ),
        'singular_name'      => _x( 'JupilerBOB-bier', 'post type singular name' ),
        'add_new'            => _x( 'Voeg nieuw toe', 'book' ),
        'add_new_item'       => __( 'Voeg een nieuw JupilerBOB-bier toe' ),
        'edit_item'          => __( 'Bewerk het JupilerBOB-bier' ),
        'new_item'           => __( 'Nieuw JupilerBOB-bier' ),
        'all_items'          => __( 'JupilerBOB-bieren' ),
        'view_item'          => __( 'Bekijk het JupilerBOB-bier' ),
        'search_items'       => __( 'Zoek een JupilerBOB-bier' ),
        'not_found'          => __( 'Geen JupilerBOB-bier gevonden' ),
        'not_found_in_trash' => __( 'Geen JupilerBOB-bier gevonden in de vuilnisbak' ),
        'parent_item_colon'  => '',
        'menu_name'          => 'Portfolio'
    );
    $args = array(
        'labels'        => $labels,
        'description'   => 'Holds our portfolio items specific data',
        'public'        => true,
        'menu_position' => 5,
        'menu_icon'     => 'dashicons-portfolio',
        'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'custom-fields'),
        'has_archive'   => true
    );
    register_post_type( 'portfolio', $args );
}
add_action( 'init', 'custom_post_type_portfolio' );

function pagination_bar() {
    global $wp_query;

    $total_pages = $wp_query->max_num_pages;

    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));

        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}

function taxonomies_portfolio() {
    $labels = array(
        'name'              => _x( 'Portfolio Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Portfolio Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Portfolio Categories' ),
        'all_items'         => __( 'All Portfolio Categories' ),
        'parent_item'       => __( 'Parent Portfolio Category' ),
        'parent_item_colon' => __( 'Parent Portfolio Category:' ),
        'edit_item'         => __( 'Edit Portfolio Category' ),
        'update_item'       => __( 'Update Portfolio Category' ),
        'add_new_item'      => __( 'Add New Portfolio Category' ),
        'new_item_name'     => __( 'New Portfolio Category' ),
        'menu_name'         => __( 'Portfolio Categories' )
    );

    $capabilities = array(
        'edit_post'          => 'edit_portfolio',
        'read_post'          => 'read_portfolio',
        'delete_post'        => 'delete_portfolio'
    );

    $args = array(
        'labels' => $labels,
        'capabilities' => $capabilities,
        'hierarchical' => true,
    );
    register_taxonomy( 'portfolio_category', 'portfolio', $args );
}
add_action( 'init', 'taxonomies_portfolio', 0 );
// init van de categorieën van portfolio