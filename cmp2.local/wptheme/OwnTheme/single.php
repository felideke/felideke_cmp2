<?php
/**
 * Created by PhpStorm.
 * User: felinedekeyser
 * Date: 21/04/17
 * Time: 10:14
 */

?>

<?php get_header(); ?>
<div class="container">
    <div class="row">

        <div class="col-md-10">
            <div class="content">

                <?php if(have_posts())
                {
                    while(have_posts())
                    {
                        //Initialize the post
                        the_post();

                        ?>
                        <h2><?php the_title(); ?></h2>
                        <div class="date">Gepubliceerd op <?php the_date(); ?> door <?php the_author(); ?></div>
                        <div class="row">
                            <?php if(has_post_thumbnail()) {
                                echo '<div class="single_thumbnail col-md-4" >';
                                the_post_thumbnail('medium');
                                echo '</div>';
                                echo '<div class="single_thumbnail col-md-8" >';
                                the_content();
                                echo '</div>';
                            }
                            else {
                                echo '<div class="single_thumbnail col-md-12" >';
                                the_content();
                                echo '</div>';
                            }; ?>
                        </div>
                        <br>
                        <p class="postmetadata">
                            <?php edit_post_link('Bewerk'); ?>

                        <?php
                        comments_template();
                        //Print the title and the content of the current post Zelfde als hierboven

                        //the_title('<h1>', '</h1>');
                        //the_excerpt();
                        //echo '<a href="';
                        //the_permalink();
                        //echo '">Lees meer</a>';
                    }
                }
                else
                {
                    echo 'No content available';
                } ?>


            </div>
        </div>
        <aside class="col-md-2">
            <?php dynamic_sidebar( 'primary-sidebar' ); ?>
        </aside>
    </div>
    </div>
<?php get_footer(); ?>