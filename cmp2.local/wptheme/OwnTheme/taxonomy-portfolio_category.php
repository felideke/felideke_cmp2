<?php
/**
 * Created by PhpStorm.
 * User: felinedekeyser
 * Date: 31/03/17
 * Time: 09:17
 */

?>
<?php get_header(); ?>

    <div class="container">
        <div class="row">

            <div class="col-md-10">

                <div class="card-columns">
                    <?php
                    if(have_posts())
                    {
                        while(have_posts()) {
                            the_post();
                            ?>
                            <div class="card">
                                <a href="<?php the_permalink(); ?>" rel="Link naar item"
                                   title="Permalink naar <?php the_title_attribute(); ?>">
                                    <div class="card-img-top img-fluid"><?php the_post_thumbnail(); ?></div>
                                </a>
                                <div class="card-block">
                                    <h4 class="card-title"><?php the_title('<h1>', '</h1>'); ?></h4>
                                    <p class="card-text"><?php the_excerpt(); ?></p>
                                    <p class="card-text">
                                        <small class="text-muted">Prijs: €<?php the_field('prijs'); ?> per flesje.</small>
                                    </p>
                                </div>
                                <div class="card-footer">
                                    <a class="btn btn-secondary" href="<?php the_permalink() ?>" rel="Link naar item" title="Permalink naar <?php the_title_attribute(); ?>">
                                        Meer info...
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    else
                    {
                        echo 'No content available';
                    } ?>
                </div>
            </div>
            <aside class="col-md-2">
                <?php dynamic_sidebar( 'primary-sidebar' ); ?>
            </aside>
        </div>
    </div>

<?php get_footer(); ?>