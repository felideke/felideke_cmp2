<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '00alcohol');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'G}=~w0;YQZU+7-Eo.,`PHiQZL!fF^Kx/Shz/gY:-0 LkysnLn10x=kF tr.[Kp#g');
define('SECURE_AUTH_KEY',  'sW|;=?vV.Q0!9/vrKqqg0G=]{8Of Zr`R9 ^kX,PmZctF^BXEqZmto@Z0O2,gp1w');
define('LOGGED_IN_KEY',    'U,r>B2/0 f&Z !2^ZRbax(ChR||3zHUcYBI>&%g6^rU}GbA3E*ycC{Y@-I@~&=g&');
define('NONCE_KEY',        ',(>oMGY!^_=xXdF,dD4ewLlAr7B~r2P<PQtXKV$ry@RyZ5O5,Y2}eu#0a<<Jknbk');
define('AUTH_SALT',        'Hb?WDD,]|z+R&DDpY%b~I6*/@M,_)nv,tD;U51y,.B0{#Z~oeKn-9atT~D#Q/iNO');
define('SECURE_AUTH_SALT', 'ur~nNR6DU?]&=ZSQiu%*D.s@HMOLNlR}!1mk0%!FseloeVi>Oqn1;x=!hm#7i??p');
define('LOGGED_IN_SALT',   'ch6ZT}V&8%9u)~9E]$P-Os@7f_,VRj>MJTr&@]4W7.13! w81BNh}L,-+y%Ffy{V');
define('NONCE_SALT',       'r2,keW:$zE7Ac3qT[f&c[tE&OYGgB!t3_~GE8wPtSde[QF2Q,h>M>p}e(yrH|/`$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
