<?php
/**
 * Created by PhpStorm.
 * User: felinedekeyser
 * Date: 5/05/17
 * Time: 22:14
 */

if(have_posts())
{
    while(have_posts())
    {
        //Initialize the post
        the_post();
        //Print the title and the content of the current post
        echo '<div class="postcontent">';
        the_content();
        echo '</div>';
    }
}
else
{
    echo 'No content available';
} ?>