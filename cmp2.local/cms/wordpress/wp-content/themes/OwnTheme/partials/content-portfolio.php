<?php
/**
 * Created by PhpStorm.
 * User: felinedekeyser
 * Date: 24/03/17
 * Time: 11:19
 */

if(have_posts())
{
    while(have_posts())
    {
        //Initialize the post
        the_post();
        //Print the title and the content of the current post
        the_date('d-m-Y', '<div class="date">', '</div>');
        the_title('<h1>', '</h1>'); // = the_title('<h1>', '</h1>', true) --> bij false niet op scherm geplaatst maar gewoon ophalen (vb in variabale steken om iets mee te doen)
        echo '<div class="portfolio">'; ?>
        <div class="row">
            <div class="col-md-5"><?php the_post_thumbnail('medium');?></div>
            <div class="col-md-7"><?php the_content();?></div>
        </div>
        <?php
        echo('<p>Prijs: ');
        the_field('prijs');
        echo(' euro per flesje.</p>');
        ?>
        <p class="postmetadata">
        <?php edit_post_link('Bewerk'); ?>
        <?php echo '</div>';
    }
}
else
{
    echo 'No content available';
} ?>