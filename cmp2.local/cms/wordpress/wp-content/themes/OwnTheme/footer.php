<?php
/**
 * Created by PhpStorm.
 * User: felinedekeyser
 * Date: 21/04/17
 * Time: 10:15
 */
?>

<div class="footer">
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <?php dynamic_sidebar('footer-sidebar');?>
        </div>
        <div class="col-md-4">
            <h3>Navigatie</h3>
            <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
        </div>
        <div class="col-md-2">
            <h3>Social media</h3>
            <?php wp_nav_menu( array( 'theme_location' => 'footer-social' ) ); ?>
        </div>
    </div>
</div>
</div>

<?php wp_footer(); ?>

</body>
</html>