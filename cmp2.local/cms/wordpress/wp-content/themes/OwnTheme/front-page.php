<?php
/**
 * Created by PhpStorm.
 * User: felinedekeyser
 * Date: 10/03/17
 * Time: 10:07
 */

?>

<?php get_header(); ?>

<div class="container">
    <div class="row">
        <div class="card-deck col-md-10">
            <p><?php get_template_part('partials/content-page'); ?></p>

            <div class="card-deck">
                <?php
                $posts = get_posts ("cat=0&showposts=3"); //id van de posts verzamelen (3) "cat=0&showposts=3"
                if ($posts) {
                foreach ($posts as $post):
                setup_postdata($post);
                ?>
                <div class="card">
                    <a href="<?php the_permalink() ?>" rel="Link naar item" title="Permalink naar <?php the_title_attribute(); ?>">
                        <div class="card-img-top"><?php the_post_thumbnail(); ?></div>
                    </a>
                    <div class="card-block">
                        <h4 class="card-title"><?php the_title(); ?></h4>
                        <p class="card-text"><?php the_excerpt(); ?></p>
                        <p class="card-text"><small class="text-muted"><?php the_date(); ?></small></p>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-secondary" href="<?php the_permalink() ?>" rel="Link naar item" title="Permalink naar <?php the_title_attribute(); ?>">
                            Meer info...
                        </a>
                    </div>
                    </div><?php
                endforeach;
                }
                ?>
                </div>
            </div>
        <aside class="col-md-2">
            <?php dynamic_sidebar( 'primary-sidebar' ); ?>
        </aside>
    </div>
</div>

<?php get_footer(); ?>

