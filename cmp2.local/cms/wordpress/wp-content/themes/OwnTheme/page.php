<?php
/**
 * Created by PhpStorm.
 * User: felinedekeyser
 * Date: 21/04/17
 * Time: 10:14
 */

?>
<?php get_header(); ?>
<div class="container">
    <div class="row">

        <div class="col-md-10">

            <div class="content">
                <?php
                get_template_part('partials/content-page');

                ?>
            </div>
        </div>

        <aside class="col-md-2">
            <?php dynamic_sidebar( 'primary-sidebar' ); ?>
        </aside>
    </div>
</div>
<?php get_footer(); ?>