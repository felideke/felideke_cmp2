<?php
/**
 * Created by PhpStorm.
 * User: felinedekeyser
 * Date: 21/04/17
 * Time: 10:33
 */

?>


<?php get_header(); ?>
<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="primary" class="content-area">
                    <div id="content" class="site-content" role="main">

                        <div class="page-header">
                            <h1 class="page-title"><?php _e( '404 NOT FOUND', 'twentythirteen' ); ?></h1>
                        </div>

                        <div class="page-wrapper">
                            <div class="page-content">
                                <h3><?php _e('OEPS HET BIER WERD NIET GEVONDEN', 'twentythirteen'); ?></h3>
                                <p><?php _e( 'Het lijkt erop dat niets werd gevonden op deze locatie.', 'twentythirteen' ); ?></p>
                            </div><!-- .page-content -->
                        </div><!-- .page-wrapper -->

                    </div><!-- #content -->
                </div><!-- #primary -->
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>