<?php
/**
 * Created by PhpStorm.
 * User: felinedekeyser
 * Date: 21/04/17
 * Time: 10:15
 */

?>
<!DOCTYPE html>
<html>
<head>
    <!-- Hier staan je eigen links en meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php wp_head(); ?>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/main.css?<?php echo time();?>">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Ubuntu">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Russo+One">
    <title>JupilerBOB</title>
</head>
<body>
<header>
    <nav class="navbar navbar-toggleable-md navbar-light bg-faded container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
            <?php
            echo('<div class="logo">');
            if ( function_exists( 'the_custom_logo' ) ) {
                the_custom_logo();
            }
            echo('</div>');
            ?>
        </a>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ml-auto">
                    <?php wp_nav_menu( array('theme_location' => 'primary-menu')); ?>
                </div>
            </div>
        </nav>
</header>

<img class="headerfoto" alt="header-image" src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>">
